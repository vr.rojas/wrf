from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()

def sfc(date):
    server.retrieve({
        'class' : "ei",
        'dataset' : "interim",
        'step' : "0",
        'levtype' : "sfc",
        'date' : date["idate"]+"/to/"+date["fdate"],
        'time' : "00/06/12/18",
        'type' : "an",
        'param' : "134.128/139.128/141.128/151.128/165.128/166.128/167.128/168.128/170.128/183.128/235.128/236.128/31.128/33.128/34.128/39.128/40.128/41.128/42.128",
        'area' : "5/-90/-22/-64",
        'grid' : "0.75/0.75",
        'stream' : "oper",
        'RESOL' : "AV",
        'target' : "ERA-sfc.grib"
        })

def ml(date):
    server.retrieve({
        'class' : "ei",
        'dataset' : "interim",
        'step' : "0",
        'levtype' : "ml",
        'levelist' : "1/to/60",
        'date' : date["idate"]+"/to/"+date["fdate"],
        'time' : "00/06/12/18",
        'type' : "an",
        'param' : "130.128/131.128/132.128/133.128",
        'area' : "5/-90/-22/-64",
        'grid' : "0.75/0.75",
        'stream' : "oper",
        'RESOL' : "AV",
        'target' : "ERA-ml.grib"
        })


def invarint(date):
    server.retrieve({
        'class' : "ei",
        'dataset' : "interim",
        'date' : date["idate"]+"/to/"+date["fdate"],
        'grid' : "0.75/0.75",
        'levtype' : "sfc",
        'param' : "129.128/172.128",
        'step' : "0",
        'stream' : "oper",
        'time' : "12:00:00",
        'type' : "an",
        'area' : "5/-90/-22/-64",
        'RESOL' : "AV",
        'target' : "ERA-invariant.grib"
        })


