
from ecmwfapi import ECMWFDataServer
import os

os.system("clear")

pathNow =os.getcwd()


## Crear nuevo proyecto
##----------------------------------------------------------------------
changeDir = "/home/wrf/build_wrf/data/eraInterim"
os.chdir(changeDir)

directories = os.listdir(changeDir)
print("Directorios actuales")
for idir in range(len(directories)):
    print(idir, ". ", directories[idir])

print("""\n\t\tCrear un nuevo directorio\n\t\tpara almacenar datos\n\t\tde entrada para WSP""")
newDir = str(input("Nombre de directorio: "))

if os.path.exists(newDir):
    pass
else:
    os.mkdir(newDir)


# Descargar datos en formato grib de ECMWFDataServer
import vrrp_era_iterim_basedata as era
print("\t\tDescargar datos de ECMWF data server\n\n")
print("Ejemplo:\nfecha incial: 20121201\nfecha final: 20140131\n\nby VR. Rojas")
idate = str(input("fecha inicial: "))
fdate = str(input("fecha final: "))
"""
date = {"idate": "20121201",
        "fdate": "20140131"}
"""

date = {"idate": idate,
        "fdate": fdate}
os.chdir(changeDir+"/"+newDir)

era.ml(date)
era.invarint(date)
era.sfc(date)
